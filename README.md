# Data Structures and Algorythms


|    |  Problem          |   Solution                                                    |
|----|-------------------|---------------------------------------------------------------|
| 1  | Two Sum           | [Python](Array/Two-Sum/twoSum.py), [C](Array/Two-Sum/twoSum.c)|
| 200| Number of Island  | [C](Matrix/Num-Islands/numIsland.c)                           |
| 217| Contains Duplicate|                                                               |
